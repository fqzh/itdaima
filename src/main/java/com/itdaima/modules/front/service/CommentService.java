package com.itdaima.modules.front.service;

import com.itdaima.common.service.CrudService;
        import com.itdaima.modules.front.dao.CommentDao;
        import com.itdaima.modules.front.entity.Comment;
        import org.springframework.stereotype.Service;
        import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2017/8/3.
 */
@Service
@Transactional(readOnly = true)
public class CommentService extends CrudService<CommentDao, Comment> {


}
