ITdaima （IT代码）是一款基于SpringMVC+Spring+Mybatis+MybatisPlus+Ehcahe的敏捷开发系统；是以Spring Framework为核心容器，Spring MVC为模型视图控制器，Mybatis为数据访问层， Apache Shiro为权限授权层，Ehcahe数据进行缓存，Bootstrap3作为前端框架的优秀 开源 系统。

1、后端

- 核心框架：Spring Framework
- 安全框架：Apache Shiro
- 视图框架：Spring MVC
- 持久层框架：Mybatis
- 数据库连接池：Alibaba Druid
- 缓存框架：Ehcache
- 日志管理：Log4j1

2、前端

JS框架：jQuery。
CSS框架：Bootstrap3
客户端验证：bootstrapvalidator。
富文本在线编辑：ckeditor
数据表格：Bootstrap-Table
对话框：layer

### 说明

导入db/itdaima_.sql脚本文件到mysql数据库
修改数据库配置文件resources/jdbc.properties 数据库账号密码
jdbc.url=jdbc:mysql://localhost:3306/itdaima_?useUnicode=true&characterEncoding=utf-8
jdbc.username=XXX
jdbc.password=###

Tomcat启动项目,默认管理员账号admin/密码admin

 _前台地址（启动默认）：http://localhost:8080/t  

后台地址： http://localhost:8080/a  _ 

﻿网站截图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0801/085945_85484db9_1006226.jpeg "QQ截图20170801090108.jpg")
增加评论功能：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0803/151916_b4811c5e_1006226.jpeg "QQ截图20170803152053.jpg")
版权itdaima所有
